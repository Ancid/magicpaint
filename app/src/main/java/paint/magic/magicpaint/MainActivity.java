package paint.magic.magicpaint;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import java.util.UUID;
import android.provider.MediaStore;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener
{
    private float smallBrush, mediumBrush, largeBrush;
    private DrawingView drawView;
    private ImageButton currPaint, drawBtn, eraseBtn, newBtn, saveBtn, colorBtn;

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        smallBrush = getResources().getInteger(R.integer.small_size);
        mediumBrush = getResources().getInteger(R.integer.medium_size);
        largeBrush = getResources().getInteger(R.integer.large_size);

        final Context context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawView = (DrawingView)findViewById(R.id.drawing);
        drawView.setBrushSize(smallBrush);
        drawView.setLastBrushSize(smallBrush);

        drawBtn = (ImageButton)findViewById(R.id.draw_btn);
        drawBtn.setOnClickListener(this);

        eraseBtn = (ImageButton)findViewById(R.id.erase_btn);
        eraseBtn.setOnClickListener(this);

        newBtn = (ImageButton)findViewById(R.id.new_btn);
        newBtn.setOnClickListener(this);

        saveBtn = (ImageButton)findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(this);

        colorBtn = (ImageButton)findViewById(R.id.color_btn);
        colorBtn.setOnClickListener(this);

        // ShakeDetector initialization
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {

            @Override
            public void onShake(int count) {
                drawView.handleShakeEvent(count);
            }
        });
    }

    public void paintClicked(View view){
        drawView.setErase(false);
        drawView.setBrushSize(drawView.getLastBrushSize());
        if(view != currPaint){
            ImageButton imgView = (ImageButton)view;
            String color = view.getTag().toString();

            drawView.setColor(color);

            imgView.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.paint_pressed));
//            currPaint.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.paint));
            currPaint=(ImageButton)view;
        }
    }

    @Override
    public void onClick(View view){
        if(view.getId() == R.id.draw_btn){
            final Dialog brushDialog = new Dialog(this);
            brushDialog.setTitle("Размер кисти:");
            brushDialog.setContentView(R.layout.brush_chooser);

            ImageButton smallBtn = (ImageButton)brushDialog.findViewById(R.id.small_brush);
            smallBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    drawView.setBrushSize(smallBrush);
                    drawView.setLastBrushSize(smallBrush);
                    drawView.setErase(false);
                    brushDialog.dismiss();
                }
            });

            ImageButton mediumBtn = (ImageButton)brushDialog.findViewById(R.id.medium_brush);
            mediumBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    drawView.setBrushSize(mediumBrush);
                    drawView.setLastBrushSize(mediumBrush);
                    drawView.setErase(false);
                    brushDialog.dismiss();
                }
            });

            ImageButton largeBtn = (ImageButton)brushDialog.findViewById(R.id.large_brush);
            largeBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    drawView.setBrushSize(largeBrush);
                    drawView.setLastBrushSize(largeBrush);
                    drawView.setErase(false);
                    brushDialog.dismiss();
                }
            });

            brushDialog.show();
        } else if(view.getId() == R.id.erase_btn){
            final Dialog brushDialog = new Dialog(this);
            brushDialog.setTitle("Eraser size:");
            brushDialog.setContentView(R.layout.brush_chooser);
            ImageButton smallBtn = (ImageButton)brushDialog.findViewById(R.id.small_brush);
            smallBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    drawView.setErase(true);
                    drawView.setBrushSize(smallBrush);
                    brushDialog.dismiss();
                }
            });
            ImageButton mediumBtn = (ImageButton)brushDialog.findViewById(R.id.medium_brush);
            mediumBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    drawView.setErase(true);
                    drawView.setBrushSize(mediumBrush);
                    brushDialog.dismiss();
                }
            });
            ImageButton largeBtn = (ImageButton)brushDialog.findViewById(R.id.large_brush);
            largeBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    drawView.setErase(true);
                    drawView.setBrushSize(largeBrush);
                    brushDialog.dismiss();
                }
            });

            brushDialog.show();
        } else if (view.getId() == R.id.save_btn) {
            AlertDialog.Builder newDialog = new AlertDialog.Builder(this);
            newDialog.setTitle("Сохранение");
            newDialog.setMessage("Сохранить рисунок в галерею?");
            newDialog.setPositiveButton("Да", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    drawView.setDrawingCacheEnabled(true);
                    String imgSaved = MediaStore.Images.Media.insertImage(
                            getContentResolver(), drawView.getDrawingCache(),
                            UUID.randomUUID().toString()+".png", "drawing");

                    if(imgSaved != null){
                        Toast savedToast = Toast.makeText(getApplicationContext(),
                                "Сохранено в галерею!", Toast.LENGTH_SHORT);
                        savedToast.show();
                    }
                    else{
                        Toast unsavedToast = Toast.makeText(getApplicationContext(),
                                "Сохрнение невозможно", Toast.LENGTH_SHORT);
                        unsavedToast.show();
                    }
                    drawView.destroyDrawingCache();
                    dialog.dismiss();
                }
            });
            newDialog.setNegativeButton("Отмена", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    dialog.cancel();
                }
            });
            newDialog.show();
        } else if (view.getId() == R.id.color_btn) {
            final Dialog colorDialog = new Dialog(this);
            colorDialog.setTitle("Выбор цвета:");
            colorDialog.setContentView(R.layout.color_chooser);

            ImageButton blueBtn = (ImageButton)colorDialog.findViewById(R.id.FF0000FF);
            blueBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    paintClicked(v);
                    colorDialog.dismiss();
                }
            });

            ImageButton redBtn = (ImageButton)colorDialog.findViewById(R.id.FFFF0000);
            redBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    paintClicked(v);
                    colorDialog.dismiss();
                }
            });

            ImageButton orgBtn = (ImageButton)colorDialog.findViewById(R.id.FFFF6600);
            orgBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    paintClicked(v);
                    colorDialog.dismiss();
                }
            });

            ImageButton yelBtn = (ImageButton)colorDialog.findViewById(R.id.FFFFCC00);
            yelBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    paintClicked(v);
                    colorDialog.dismiss();
                }
            });

            ImageButton grnBtn = (ImageButton)colorDialog.findViewById(R.id.FF009900);
            grnBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    paintClicked(v);
                    colorDialog.dismiss();
                }
            });

            ImageButton magBtn = (ImageButton)colorDialog.findViewById(R.id.FF009999);
            magBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    paintClicked(v);
                    colorDialog.dismiss();
                }
            });

            ImageButton brBtn = (ImageButton)colorDialog.findViewById(R.id.FF660000);
            brBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    paintClicked(v);
                    colorDialog.dismiss();
                }
            });

            ImageButton prpBtn = (ImageButton)colorDialog.findViewById(R.id.FF990099);
            prpBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    paintClicked(v);
                    colorDialog.dismiss();
                }
            });

            ImageButton pnkBtn = (ImageButton)colorDialog.findViewById(R.id.FFFF6666);
            pnkBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    paintClicked(v);
                    colorDialog.dismiss();
                }
            });

            ImageButton whBtn = (ImageButton)colorDialog.findViewById(R.id.FFFFFFFF);
            whBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    paintClicked(v);
                    colorDialog.dismiss();
                }
            });

            ImageButton greyBtn = (ImageButton)colorDialog.findViewById(R.id.FF787878);
            greyBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    paintClicked(v);
                    colorDialog.dismiss();
                }
            });

            ImageButton blkBtn = (ImageButton)colorDialog.findViewById(R.id.FF000000);
            blkBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    paintClicked(v);
                    colorDialog.dismiss();
                }
            });

            colorDialog.show();
        } else if(view.getId() == R.id.new_btn){
            AlertDialog.Builder newDialog = new AlertDialog.Builder(this);
            newDialog.setTitle("Новый рисунок");
            newDialog.setMessage("Начать новый рисунок (текущий будет утерян)?");
            newDialog.setPositiveButton("Да", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    drawView.startNew();
                    dialog.dismiss();
                }
            });
            newDialog.setNegativeButton("Отмена", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    dialog.cancel();
                }
            });
            newDialog.show();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        // Add the following line to register the Session Manager Listener onResume
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {
        // Add the following line to unregister the Sensor Manager onPause
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
    }
}
